import React from 'react';
import {
	StyleSheet,
	ScrollView,
	View,
	Button,
} from 'react-native';

import {
	Colors,
} from 'react-native/Libraries/NewAppScreen';

import { ListItem } from 'react-native-elements'
const DataList = [
	{
	  id: '20191215',
	  content: '飲料機投不到蜜豆奶',
	  status: '待回覆'
	},
	{
		id: '20191215',
		content: ' 飲料機投不到蜜豆奶',
		status: '待回覆'
	},
]

export default class FeedBackList extends React.Component {

    render(){
        return (
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.scrollView}>
                <View style={styles.body}>
					{
						DataList.map((l, i) => (
							<ListItem
								key={i}
								leftElement={l.id}
								title={l.content}
								rightElement={()=>{
									return (
										<Button title={l.status}></Button>
									);
								}}
								bottomDivider
							/>
						))
					}
                </View>
            </ScrollView>
        )
    } 
};


const styles = StyleSheet.create({
	scrollView: {
		backgroundColor: Colors.lighter,
	},
	engine: {
		position: 'absolute',
		right: 0,
	},
	body: {
		backgroundColor: Colors.white,
	},
	sectionContainer: {
		marginTop: 32,
		paddingHorizontal: 24,
	},
	sectionTitle: {
		fontSize: 24,
		fontWeight: '600',
		color: Colors.black,
	},
	sectionDescription: {
		marginTop: 8,
		fontSize: 18,
		fontWeight: '400',
		color: Colors.dark,
	},
	highlight: {
		fontWeight: '700',
	},
	footer: {
		color: Colors.dark,
		fontSize: 12,
		fontWeight: '600',
		padding: 4,
		paddingRight: 12,
		textAlign: 'right',
	},

});
